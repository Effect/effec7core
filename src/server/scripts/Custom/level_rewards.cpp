#include "ScriptPCH.h"

enum LevelRewards
{
	EMBLEM_OF_TRIUMPH = 47241,
	EMBLEM_OF_FROST = 49426,
	EMBLEM_OF_COMQUEST = 45624,
};

class level_rewards : public PlayerScript
{
   public:
	   level_rewards() : PlayerScript("level_rewards") {}

	   void OnLevelChanged(Player* player, uint8 oldLevel)
	   {
		   if (player->IsGameMaster())
			   return;

		   uint32 money = 0;

		   switch (oldLevel)
		   {
		      case 9:
				  money = 10000000;
				  break;
			  case 19:
				  money = 15000000;
				  break;
			  case 29:
				  money = 20000000;
				  break;
			  case 39:
				  money = 25000000;
				  break;
			  case 49:
				  money = 30000000;
				  break;
			  case 59:
				  money = 35000000;
				  break;
			  case 69:
				  money = 40000000;
				  break;
			  case 79:
				  money = 45000000;
				  break;
			  default:
				  return;
		   }

		   MailSender sender(MAIL_NORMAL, 0, MAIL_STATIONERY_GM);

		   MailDraft draft("Gratulujem.", "Dakujeme ze ste presli na nas server, zasluzite si odmenu. :)");

		   SQLTransaction trans = CharacterDatabase.BeginTransaction();

		   if (money > 0)
			   draft.AddMoney(money);

		   if (oldLevel == 79)
		   {
			   Item* item = Item::CreateItem(EMBLEM_OF_FROST, 300, 0);
			   item->SaveToDB(trans);
			   draft.AddItem(item);
		   }

		   draft.SendMailTo(trans, MailReceiver(player), sender);
		   CharacterDatabase.CommitTransaction(trans);
	   }
};

void AddSC_level_rewards()
{
	new level_rewards();
}