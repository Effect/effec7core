﻿#include "ScriptPCH.h"
#include "Channel.h"

class System_Censure : public PlayerScript
{
public:
	System_Censure() : PlayerScript("System_Censure") {}

	void OnChat(Player* player, uint32 /*type*/, uint32 lang, std::string& msg)
	{
		CheckMessage(player, msg, lang, NULL, NULL, NULL, NULL);
	}

	void OnChat(Player* player, uint32 /*type*/, uint32 lang, std::string& msg, Player* receiver)
	{
		CheckMessage(player, msg, lang, receiver, NULL, NULL, NULL);
	}

	void OnChat(Player* player, uint32 /*type*/, uint32 lang, std::string& msg, Group* group)
	{
		CheckMessage(player, msg, lang, NULL, group, NULL, NULL);
	}

	void OnChat(Player* player, uint32 /*type*/, uint32 lang, std::string& msg, Guild* guild)
	{
		CheckMessage(player, msg, lang, NULL, NULL, guild, NULL);
	}

	void OnChat(Player* player, uint32 /*type*/, uint32 lang, std::string& msg, Channel* channel)
	{
		CheckMessage(player, msg, lang, NULL, NULL, NULL, channel);
	}

	void CheckMessage(Player* player, std::string& msg, uint32 lang, Player* /*receiver*/, Group* /*group*/, Guild* /*guild*/, Channel* channel)
	{
		if (player->IsGameMaster() || lang == LANG_ADDON)
			return;

		// transform to lowercase (for simpler checking)
		std::string lower = msg;
		std::transform(lower.begin(), lower.end(), lower.begin(), ::tolower);

		const uint8 cheksSize = 67; 
		std::string checks[cheksSize];
		// Strony (Sites)
		checks[0] = "http://";
		checks[1] = ".com";
		checks[2] = ".www";
		checks[3] = ".net";
		checks[4] = ".org";
		checks[5] = ".ru";
		checks[6] = "www.";
		checks[7] = "wow-";
		checks[8] = "-wow";
		checks[9] = ".pl";
		checks[10] = ".sk";
		checks[11] = ".eu";
		checks[12] = ".cz";
		// Polskie przekle�stwa (Polish curses)
		checks[13] = "chuj";
		checks[14] = "huj";
		checks[15] = "kurw";
		checks[16] = "kurwa";
		checks[17] = "jeba";
		checks[18] = "cipa";
		checks[19] = "gej";
		checks[20] = "cwel";
		checks[21] = "pizd";
		checks[22] = "pierdo";
		checks[23] = "spierd";
		checks[24] = "zjeb";
		checks[25] = "wypierdalaj";
		checks[26] = "kutas";
		// English curses
		checks[27] = "bitch";
		checks[28] = "clit";
		checks[29] = "cock";
		checks[30] = "cum";
		checks[31] = "cunt";
		checks[32] = "dick";
		checks[33] = "faggot";
		checks[34] = "fuck";
		checks[35] = "gay";
		checks[36] = "lesbian";
		checks[38] = "penis";
		checks[39] = "prick";
		checks[40] = "slut";
		checks[41] = "twat";
		checks[42] = "whore";
		// sloval curses
		checks[43] = "kokot";
		checks[44] = "piča";
		checks[45] = "pica";
		checks[46] = "buzna";
		checks[47] = "kurva";
		checks[48] = "zmrd";
		checks[49] = "jebo";
		checks[50] = "geňo";
		checks[51] = "geno";
		checks[52] = "skurvysyn";
		checks[53] = "skurvisin";
		checks[54] = "skurvysin";
		checks[55] = "skurvisyn";
		checks[56] = "zkurvysyn";
		checks[57] = "zkurvisin";
		checks[58] = "zkurvysin";
		checks[59] = "zkurvisyn";
		checks[60] = "kokoot";
		checks[61] = "smrdiš";
		checks[62] = "smrdis";
		checks[63] = "deges";
		checks[64] = "degeš";
		checks[65] = "picus";
		checks[66] = "pičus";
 
		for (int i = 0; i < cheksSize; ++i)
		if (lower.find(checks[i]) != std::string::npos)
		{
			msg = "";
			ChatHandler(player->GetSession()).PSendSysMessage("Reklama a hrubé správanie nie je dovolené!");
			return;
		}
	}
};

void AddSC_System_Censure()
{
	new System_Censure();
}