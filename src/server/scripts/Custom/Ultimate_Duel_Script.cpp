/*	
Ultimate Duel Script (Reward/Rating/Security/Zoned & Reset)
Author : Philippe
Version : V1.4 (Full Rewrite)
Release Date : 21/03/14
Script Complete : 80 %
Version : 3.3.5 & 4.3.4
TrinityCore based.
Tested and Works Well.

Phasing System is in rewrite state. (V1.5)

Note :
For setup, Please follow instruction in readme.txt file. 
or on thoses thread
AC-Web : http://www.ac-web.org/forums/showthread.php?195691-Ultimate-Duel-Script
Emudevs : http://emudevs.com/showthread.php/2390-Wotlk-Cata-Ultimate-Duel-Script
*/
#include "Ultimate_Duel_Script.h"

class Ultimate_Duel_Script : public PlayerScript
{
public:
	Ultimate_Duel_Script() : PlayerScript("Ultimate_Duel_Script") {}

	void OnDuelStart(Player* player, Player* playerTarget)
	{	
		// Resewing HP/MANA-ENERGY/
		RevivePlayerStart(player);
		RevivePlayerStart(playerTarget);

		// Check if rated system is active & check if they are in the good area
		if(isActivated(player,playerTarget))
		{
			RatingInfo(player);
			RatingInfo(playerTarget);
		}
	}

	void OnDuelEnd(Player* player, Player* playerTarget,  DuelCompleteType type)
	{
		// Resewing HP/MANA-ENERGY/COOLDOWN
		RevivePlayerEnd(player);
		RevivePlayerEnd(playerTarget);

		// Check if rated system is active & check if they are in the good area
		if(isActivated(player,playerTarget))
		{	// Checking if player is trying to auto-farm
			if(isSecurityActive(player,playerTarget) && (type == DUEL_WON || type == DUEL_INTERRUPTED || type == DUEL_FLED))
			{ return; } // Do not remove
			Update_Stats(player,playerTarget);
		}
		// Check again if Security is active for Token Reward (It's need to be checked another time if user don't want to use rated system)
		if(isSecurityActive(player,playerTarget) && (type == DUEL_WON || type == DUEL_INTERRUPTED || type == DUEL_FLED))
		{ return; } // Do not remove
		Reward_Token(player,playerTarget);
	}
};

void AddSC_Ultimate_Duel_Script()
{
	new Ultimate_Duel_Script();
}