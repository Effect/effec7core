#include "Pet.h"
#include "Config.h"

enum Misc
{
	// Player Level Check
	MIN_LEVEL_REQ = 80,
	// Token Reward Default Setup || Can be change by using config
	TOKEN_LOSER = 1, // Default
	TOKEN_WINNER = 3, // Default
	ITEMORCURRENCY_REWARD_ID = 0, // Default

	// Minimum Gear Stats REQ
#if GEAR_STAMINA_REQ == 1
	GEAR_STAMINA_REQ  = 4000,	
#else // WotLK
	GEAR_STAMINA_REQ  = 1750,	
#endif 
};

bool isAreaValide(Player* player)
{		
	// Maps Check (Only for Rating system) || Can be NULL please make sure to configure 
	uint32 ZONE_1 = sConfigMgr->GetIntDefault("Rated_Duel.Zone_1", NULL); uint32 AREA_1 = sConfigMgr->GetIntDefault("Rated_Duel.Area_1", NULL);
	uint32 ZONE_2 = sConfigMgr->GetIntDefault("Rated_Duel.Zone_2", NULL); uint32 AREA_2 = sConfigMgr->GetIntDefault("Rated_Duel.Area_2", NULL);
	uint32 ZONE_3 = sConfigMgr->GetIntDefault("Rated_Duel.Zone_3", NULL); uint32 AREA_3 = sConfigMgr->GetIntDefault("Rated_Duel.Area_3", NULL);
	uint32 ZONE_4 = sConfigMgr->GetIntDefault("Rated_Duel.Zone_4", NULL); uint32 AREA_4 = sConfigMgr->GetIntDefault("Rated_Duel.Area_4", NULL);
	if ((player->GetZoneId() == ZONE_1 || player->GetZoneId() == ZONE_2 || player->GetZoneId() == ZONE_3 || player->GetZoneId() == ZONE_4)  
		|| (player->GetAreaId() == AREA_1 || player->GetAreaId() == AREA_2 || player->GetAreaId() == AREA_3 || player->GetAreaId() == AREA_4))
	{ return true; } // boolean need to return to a value
	return false; // boolean need to return to a value
}

bool isActivated(Player* player,Player* playerTarget)
{
	uint32 isActivated = sConfigMgr->GetIntDefault("Duel_Reward.Rating", 1);
	if(isActivated && (isAreaValide(player) && isAreaValide(playerTarget)))
	{ return true; } // boolean need to return to a value
	return false; // boolean need to return to a value
}

// Rating Info || Config Setup 
void RatingInfo(Player* player)
{
	QueryResult result = CharacterDatabase.PQuery("SELECT duelW,duelL,duelR FROM characters WHERE guid = '%u'", player->GetGUIDLow());
	if(!result)
		return;

	Field * fields = result->Fetch();
	uint32 duelW = fields[0].GetUInt32();
	uint32 duelL = fields[1].GetUInt32();
	uint32 duelR = fields[2].GetUInt32();
	ChatHandler(player->GetSession()).PSendSysMessage("[System Information] - [Duel Stats] : |cffFFFF00%u |cFF90EE90Duel win & |cffFFFF00%u |cFF90EE90Duel lose  |cffff6060[Rating] : |cffFFFF00%u \n", duelW,duelL,duelR);
}

// Resewing Health & Mana || Energy at start.
void RevivePlayerStart(Player* player)
{
	player->SetHealth(player->GetMaxHealth());
	if(player->getPowerType() == POWER_MANA)
		player->SetPower(POWER_MANA, player->GetMaxPower(POWER_MANA));
	if(player->getPowerType() == POWER_ENERGY)
		player->SetPower(POWER_ENERGY, player->GetMaxPower(POWER_ENERGY));
}

// Resewing Health & Mana || Energy, Cooldown & stopcombat at End of the duel.
void RevivePlayerEnd(Player* player)
{
	RevivePlayerStart(player);
	player->CombatStop();
	player->RemoveAllSpellCooldown();
}

// Check if player is cheating
bool isSecurityActive(Player* player, Player* playerTarget)
{
	if(sConfigMgr->GetIntDefault("Duel_Reward.Security", 1)) 
	{
		if (player->getLevel() != MIN_LEVEL_REQ || playerTarget->getLevel() != MIN_LEVEL_REQ) {
			std::ostringstream ss;
			ss << "|cFFFFFC00[Information System]|r : |cFF00FFFF You or Your Opponent Need to be both level " << MIN_LEVEL_REQ << " to be eligible for a rated duel|r \n";					
			ChatHandler(player->GetSession()).SendSysMessage(ss.str().c_str());				
			ChatHandler(playerTarget->GetSession()).SendSysMessage(ss.str().c_str());
			return true; }
		if (player->GetHealth() > 10 || playerTarget->GetStat(STAT_STAMINA) < GEAR_STAMINA_REQ) {
			std::ostringstream ss;
			ss << "|cFFFFFC00[Information System]|r : |cFF00FFFF You or Your Opponent has surrender or does not have enough gear to be eligible for a rated duel|r \n";					
			ChatHandler(player->GetSession()).SendSysMessage(ss.str().c_str());				
			ChatHandler(playerTarget->GetSession()).SendSysMessage(ss.str().c_str());
			return true; }
		if (player->GetSession()->GetSecurity() != 1 || playerTarget->GetSession()->GetSecurity() != 1) {
			std::ostringstream ss;
			ss << "|cFFFFFC00[Information System]|r : |cFF00FFFF Can't be rewarded if the fight is versus a GameMaster |r \n";					
			ChatHandler(player->GetSession()).SendSysMessage(ss.str().c_str());				
			ChatHandler(playerTarget->GetSession()).SendSysMessage(ss.str().c_str());
			return true; }
		if (player->GetSession()->GetRemoteAddress() == playerTarget->GetSession()->GetRemoteAddress()) {
			std::ostringstream ss;
			ss << "|cFFFFFC00[Information System]|r : |cFF00FFFF The Multi-Housing is not eligible for awards. |r \n";					
			ChatHandler(player->GetSession()).SendSysMessage(ss.str().c_str());				
			ChatHandler(playerTarget->GetSession()).SendSysMessage(ss.str().c_str()); }
	}
	return false;
}

// Stats Updater Winner/Loser
void Update_Stats(Player* player, Player* playerTarget)
{
	// Winner
	CharacterDatabase.PExecute("UPDATE characters SET duelW = (duelW+1) WHERE guid = '%u'", player->GetGUIDLow());
	CharacterDatabase.PExecute("UPDATE characters SET duelR = (duelR+14) WHERE guid = '%u'", player->GetGUIDLow()); 
	ChatHandler(player->GetSession()).SendSysMessage("|cFFFFFC00[System]|cFF00FFFF Well done you won 14 Points !");
	// Loser
	CharacterDatabase.PExecute("UPDATE characters SET duelL = (duelL+1) WHERE guid = '%u'", playerTarget->GetGUIDLow());
	CharacterDatabase.PExecute("UPDATE characters SET duelR = (duelR-7) WHERE guid = '%u'", playerTarget->GetGUIDLow());
	ChatHandler(playerTarget->GetSession()).SendSysMessage("|cFFFFFC00[System]|cFF00FFFF Ow you lose 7 Points !");
}

void Reward_Token(Player* player, Player* playerTarget)
{
	if(sConfigMgr->GetIntDefault("Duel_Reward.isEnable", 1)) {
#if REWARD_CATA == 1
		player->ModifyCurrency(sConfigMgr->GetIntDefault("Duel_Reward.ItemOrCurrencyID", ITEMORCURRENCY_REWARD_ID), sConfigMgr->GetIntDefault("Duel_Reward.ItemCount_Winner", TOKEN_WINNER), true, true);
		playerTarget->ModifyCurrency(sConfigMgr->GetIntDefault("Duel_Reward.ItemOrCurrencyID", ITEMORCURRENCY_REWARD_ID), sConfigMgr->GetIntDefault("Duel_Reward.ItemCount_loser", TOKEN_LOSER), true, true);
#else // WotLK
		player->AddItem(sConfigMgr->GetIntDefault("Duel_Reward.ItemOrCurrencyID", ITEMORCURRENCY_REWARD_ID), sConfigMgr->GetIntDefault("Duel_Reward.ItemCount_Winner", TOKEN_WINNER));
		playerTarget->AddItem(sConfigMgr->GetIntDefault("Duel_Reward.ItemOrCurrencyID", ITEMORCURRENCY_REWARD_ID), sConfigMgr->GetIntDefault("Duel_Reward.ItemCount_loser", TOKEN_LOSER));	
#endif
	}
}
