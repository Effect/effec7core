DELETE FROM rbac_permissions WHERE id = 778;
INSERT INTO rbac_permissions VALUES (778, 'Command: reload creature_outfits');

DELETE FROM rbac_linked_permissions WHERE linkedId = 778;
INSERT INTO rbac_linked_permissions VALUES (192, 778);