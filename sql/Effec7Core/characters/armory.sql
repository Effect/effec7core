DROP TABLE IF EXISTS `armory_character_stats`;
CREATE TABLE `armory_character_stats` (
  `guid` INT(11) NOT NULL,
  `data` LONGTEXT NOT NULL,
  `save_date` INT(11) DEFAULT NULL,
  PRIMARY KEY  (`guid`)
) ENGINE=MYISAM DEFAULT CHARSET=utf8 COMMENT='World of Warcraft Armory table';

DROP TABLE IF EXISTS `character_feed_log`;
CREATE TABLE `character_feed_log` (
  `guid` INT(11) NOT NULL,
  `type` SMALLINT(1) NOT NULL,
  `data` INT(11) NOT NULL,
  `date` INT(11) DEFAULT NULL,
  `counter` INT(11) NOT NULL,
  `difficulty` SMALLINT(6) DEFAULT '-1',
  `item_guid` INT(11) DEFAULT '-1',
  `item_quality` SMALLINT(6) NOT NULL DEFAULT '-1'
) ENGINE=MYISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `armory_game_chart`;
CREATE TABLE `armory_game_chart` (
  `gameid` INT(11) NOT NULL,
  `teamid` INT(11) NOT NULL,
  `guid` INT(11) NOT NULL,
  `changeType` INT(11) NOT NULL,
  `ratingChange` INT(11) NOT NULL,
  `teamRating` INT(11) NOT NULL,
  `damageDone` INT(11) NOT NULL,
  `deaths` INT(11) NOT NULL,
  `healingDone` INT(11) NOT NULL,
  `damageTaken` INT(11) NOT NULL,
  `healingTaken` INT(11) NOT NULL,
  `killingBlows` INT(11) NOT NULL,
  `mapId` INT(11) NOT NULL,
  `start` INT(11) NOT NULL,
  `end` INT(11) NOT NULL,
  PRIMARY KEY  (`gameid`,`teamid`,`guid`)
) ENGINE=MYISAM DEFAULT CHARSET=utf8 COMMENT='WoWArmory Game Chart';