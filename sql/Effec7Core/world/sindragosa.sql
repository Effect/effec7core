-- Sindragosa Flight
UPDATE `creature_template` SET `InhabitType`=7 WHERE `entry`=36853;

-- Register spell-script for Sindragosa ability Permeating Chill /
DELETE FROM `spell_script_names` WHERE `spell_id` = 70107 AND `ScriptName` = 'spell_sindragosa_permeating_chill';

-- Close entrance door by default in encounter Sindragosa in instance / 
UPDATE `gameobject` SET `state` = 1 WHERE `id` = 201373; 
