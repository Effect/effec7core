-- Implement Fast Arena Start
DELETE FROM `gameobject_template` WHERE `entry` = 42000;
INSERT INTO `gameobject_template`(`entry`, `type`, `displayId`, `name`, `IconName`, `size`, `ScriptName`, `WDBVerified`) VALUES (42000, 10, 327, 'Faster Start', 'PVP' , 1, 'fast_arena_start', 12340);

