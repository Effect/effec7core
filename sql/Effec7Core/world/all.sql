-- Fix Animal Handler
DELETE FROM `spell_pet_auras` WHERE `aura` = 68361;
INSERT INTO `spell_pet_auras` VALUES
(34453, 1, 0, 68361),
(34454, 1, 0, 68361);-- --------------------------------------------------------
-- Host:                         localhost
-- Server version:               5.6.10 - MySQL Community Server (GPL)
-- Server OS:                    Win64
-- HeidiSQL Version:             8.0.0.4491
-- --------------------------------------------------------
 
/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
 
-- Dumping structure for table e2_w.creature_template_outfits
CREATE TABLE IF NOT EXISTS `creature_template_outfits` (
  `entry` INT(10) UNSIGNED NOT NULL,
  `race` TINYINT(3) UNSIGNED NOT NULL DEFAULT '1',
  `gender` TINYINT(3) UNSIGNED NOT NULL DEFAULT '0' COMMENT '0 for male, 1 for female',
  `skin` TINYINT(3) UNSIGNED NOT NULL DEFAULT '0',
  `face` TINYINT(3) UNSIGNED NOT NULL DEFAULT '0',
  `hair` TINYINT(3) UNSIGNED NOT NULL DEFAULT '0',
  `haircolor` TINYINT(3) UNSIGNED NOT NULL DEFAULT '0',
  `facialhair` TINYINT(3) UNSIGNED NOT NULL DEFAULT '0',
  `head` INT(10) UNSIGNED NOT NULL DEFAULT '0',
  `shoulders` INT(10) UNSIGNED NOT NULL DEFAULT '0',
  `body` INT(10) UNSIGNED NOT NULL DEFAULT '0',
  `chest` INT(10) UNSIGNED NOT NULL DEFAULT '0',
  `waist` INT(10) UNSIGNED NOT NULL DEFAULT '0',
  `legs` INT(10) UNSIGNED NOT NULL DEFAULT '0',
  `feet` INT(10) UNSIGNED NOT NULL DEFAULT '0',
  `wrists` INT(10) UNSIGNED NOT NULL DEFAULT '0',
  `hands` INT(10) UNSIGNED NOT NULL DEFAULT '0',
  `back` INT(10) UNSIGNED NOT NULL DEFAULT '0',
  `tabard` INT(10) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=INNODB DEFAULT CHARSET=utf8;
 
 DELETE FROM command WHERE permission = 778;
 INSERT INTO command VALUES ('reload creature_outfits', 778, '');
 
-- Data exporting was unselected.
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;/* Table structure for table `dynamic_teleporter` */

CREATE TABLE `dynamic_teleporter` (
  `entry` INT(11) UNSIGNED NOT NULL,
  `menu_parent` INT(11) UNSIGNED NOT NULL DEFAULT '0',
  `menu_sub` INT(11) NOT NULL DEFAULT '-1',
  `icon` TINYINT(3) UNSIGNED NOT NULL DEFAULT '0',
  `faction` INT(11) UNSIGNED NOT NULL DEFAULT '0',
  `name` CHAR(100) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `map` SMALLINT(5) UNSIGNED NOT NULL DEFAULT '0',
  `position_x` FLOAT NOT NULL DEFAULT '0',
  `position_y` FLOAT NOT NULL DEFAULT '0',
  `position_z` FLOAT NOT NULL DEFAULT '0',
  `position_o` FLOAT NOT NULL DEFAULT '0',
  PRIMARY KEY (`entry`)
) ENGINE=MYISAM DEFAULT CHARSET=utf8 CHECKSUM=1 DELAY_KEY_WRITE=1 ROW_FORMAT=DYNAMIC;-- Implement Fast Arena Start
DELETE FROM `gameobject_template` WHERE `entry` = 42000;
INSERT INTO `gameobject_template`(`entry`, `type`, `displayId`, `name`, `IconName`, `size`, `ScriptName`, `WDBVerified`) VALUES (42000, 10, 327, 'Faster Start', 'PVP' , 1, 'fast_arena_start', 12340);

SET @ENTRY = (SELECT MAX(entry)+1 FROM creature_template);
SET @MENU_ID = (SELECT MAX(menu_id)+1 FROM gossip_menu_option);

INSERT INTO `creature_template` (`entry`, `modelid1`, `name`, `subname`, `IconName`, `gossip_menu_id`, `minlevel`, `maxlevel`, `exp`, `faction_A`, `faction_H`, `npcflag`, `speed_walk`, `speed_run`, `scale`, `rank`, `mindmg`, `maxdmg`, `dmgschool`, `attackpower`, `dmg_multiplier`, `baseattacktime`, `rangeattacktime`, `unit_class`, `unit_flags`, `unit_flags2`, `dynamicflags`, `family`, `trainer_type`, `trainer_spell`, `trainer_class`, `trainer_race`, `minrangedmg`, `maxrangedmg`, `rangedattackpower`, `type`, `type_flags`, `lootid`, `pickpocketloot`, `skinloot`, `PetSpellDataId`, `VehicleId`, `mingold`, `maxgold`, `AIName`, `MovementType`, `InhabitType`, `HoverHeight`, `Health_mod`, `Mana_mod`, `Armor_mod`, `RacialLeader`, `movementId`, `RegenHealth`, `mechanic_immune_mask`, `flags_extra`, `ScriptName`, `WDBVerified`) VALUES
(@ENTRY, 1298, "Herbert", "MultiVendor", NULL, @MENU_ID, 10, 10, 0, 35, 35, 129, 1, 1.14286, 1, 0, 13, 17, 0, 42, 1, 1500, 0, 1, 512, 2048, 8, 0, 0, 0, 0, 0, 9, 13, 100, 7, 138412032, 0, 0, 0, 0, 0, 0, 0, '', 0, 3, 1, 1, 1, 1, 0, 0, 1, 0, 2, '', 0);

INSERT INTO `gossip_menu_option` (`menu_id`, `id`, `option_icon`, `option_text`, `option_id`, `npc_option_npcflag`, `action_menu_id`, `action_poi_id`, `box_coded`, `box_money`, `box_text`) VALUES
(@MENU_ID, 0, 4, 'VendorTest 465', 3, 128, 465, 0, 0, 0, ''),
(@MENU_ID, 1, 9, 'VendorTest 54', 3, 128, 54, 0, 0, 0, ''),
(@MENU_ID, 2, 6, 'VendorTest 35574', 3, 128, 35574, 0, 0, 100, 'These goods are special, so pay up!');DELETE FROM `spell_script_names` WHERE `spell_id` IN (19591,35695,61013,61017,61697,61783);
INSERT INTO `spell_script_names` (`spell_id` ,`ScriptName`) VALUES
(19591, 'spell_gen_pet_calculate'),
(35695, 'spell_gen_pet_calculate'),
(61013, 'spell_gen_pet_calculate'),
(61017, 'spell_gen_pet_calculate'),
(61697, 'spell_gen_pet_calculate'),
(61783, 'spell_gen_pet_calculate');

DELETE FROM `creature_template_addon` WHERE `entry` = 29264;
INSERT INTO `creature_template_addon` (`entry`,`path_id`,`mount`,`bytes1`,`bytes2`,`emote`,`auras`) VALUES
(29264,0,0,0,1,0,'61783'); -- Spirit Wolf

DELETE FROM `spell_linked_spell` WHERE `spell_trigger` =54566;
INSERT INTO `spell_linked_spell` (`spell_trigger` ,`spell_effect` ,`type` ,`comment`) VALUES
(54566,61697,0,'Death Knight Pet Scaling 03'); -- Risen Ghoul

DELETE FROM `creature_template_addon` WHERE `entry` =30230;
INSERT INTO `creature_template_addon` (`entry`,`path_id`,`mount`,`bytes1`,`bytes2`,`emote`,`auras`) VALUES
(30230,0,0,0,1,0,'61697'); -- Risen Ally

DELETE FROM `creature_template_addon` WHERE `entry` =26125;
INSERT INTO `creature_template_addon` (`entry`,`path_id`,`mount`,`bytes1`,`bytes2`,`emote`,`auras`) VALUES
(26125,0,0,0,1,0,'61697'); -- Risen GhoulINSERT INTO `creature_template` (`entry`, `modelid1`, `modelid2`, `modelid3`, `modelid4`, `name`, `subname`, `IconName`, `gossip_menu_id`, `minlevel`, `maxlevel`, `exp`, `faction_A`, `faction_H`, `npcflag`, `speed_walk`, `speed_run`, `scale`, `rank`, `mindmg`, `maxdmg`, `dmgschool`, `attackpower`, `dmg_multiplier`, `baseattacktime`, `rangeattacktime`, `unit_class`, `unit_flags`, `unit_flags2`, `dynamicflags`, `family`, `trainer_type`, `trainer_spell`, `trainer_class`, `trainer_race`, `minrangedmg`, `maxrangedmg`, `rangedattackpower`, `type`, `type_flags`, `lootid`, `pickpocketloot`, `skinloot`, `PetSpellDataId`, `VehicleId`, `mingold`, `maxgold`, `AIName`, `MovementType`, `InhabitType`, `HoverHeight`, `Health_mod`, `Mana_mod`, `Armor_mod`, `RacialLeader`, `movementId`, `RegenHealth`, `mechanic_immune_mask`, `flags_extra`, `ScriptName`, `WDBVerified`) VALUES (190011, 20988, 0, 0, 0, 'Thaumaturge Vashreen', 'Arcane Reforger', NULL, 0, 80, 80, 2, 35, 35, 1, 1, 1.14286, 1, 0, 500, 500, 0, 350, 1, 2000, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 7, 138936390, 0, 0, 0, 0, 0, 0, 0, '', 0, 3, 1, 1, 1, 1, 0, 0, 1, 0, 0, 'REFORGER_NPC', 0);
-- Sindragosa Flight
UPDATE `creature_template` SET `InhabitType`=7 WHERE `entry`=36853;

-- Register spell-script for Sindragosa ability Permeating Chill /
DELETE FROM `spell_script_names` WHERE `spell_id` = 70107 AND `ScriptName` = 'spell_sindragosa_permeating_chill';

-- Close entrance door by default in encounter Sindragosa in instance / 
UPDATE `gameobject` SET `state` = 1 WHERE `id` = 201373; 
SET
@Entry = 190010,
@Name = "Warpweaver";

INSERT INTO `creature_template` (`entry`, `modelid1`, `modelid2`, `name`, `subname`, `IconName`, `gossip_menu_id`, `minlevel`, `maxlevel`, `exp`, `faction_A`, `faction_H`, `npcflag`, `scale`, `rank`, `mindmg`, `maxdmg`, `dmgschool`, `attackpower`, `dmg_multiplier`, `baseattacktime`, `rangeattacktime`, `unit_class`, `unit_flags`, `minrangedmg`, `maxrangedmg`, `rangedattackpower`, `type`, `type_flags`, `lootid`, `pickpocketloot`, `skinloot`, `AIName`, `MovementType`, `InhabitType`, `HoverHeight`, `Health_mod`, `Mana_mod`, `Armor_mod`, `RacialLeader`, `movementId`, `RegenHealth`, `mechanic_immune_mask`, `flags_extra`, `ScriptName`, `WDBVerified`) VALUES
(@Entry, 19646, 0, @Name, "Transmogrifier", NULL, 0, 80, 80, 2, 35, 35, 1, 1, 0, 500, 500, 0, 350, 1, 2000, 0, 1, 0, 0, 0, 0, 7, 0, 0, 0, 0, '', 0, 3, 1, 1, 1, 1, 0, 0, 1, 0, 0, 'Creature_Transmogrify', 0);
SET @TEXT_ID := 50000;
INSERT INTO `npc_text` (`ID`, `text0_0`, `WDBVerified`) VALUES
(@TEXT_ID, 'Transmogrification allows you to change how your items look like without changing the stats of the items.\r\nItems used in transmogrification are no longer refundable, tradeable and are bound to you.\r\nUpdating a menu updates the view and prices.\r\n\r\nNot everything can be transmogrified with eachother.\r\nRestrictions include but are not limited to:\r\nOnly armor and weapons can be transmogrified\r\nGuns, bows and crossbows can be transmogrified with eachother\r\nFishing poles can not be transmogrified\r\nYou must be able to equip both items used in the process.\r\n\r\nTransmogrifications stay on your items as long as you own them.\r\nIf you try to put the item in guild bank or mail it to someone else, the transmogrification is stripped.\r\n\r\nYou can also remove transmogrifications for free at the transmogrifier.', 1),
(@TEXT_ID+1, 'You can save your own transmogrification sets.\r\n\r\nTo save, first you must transmogrify your equipped items.\r\nThen when you go to the set management menu and go to save set menu,\r\nall items you have transmogrified are displayed so you see what you are saving.\r\nIf you think the set is fine, you can click to save the set and name it as you wish.\r\n\r\nTo use a set you can click the saved set in the set management menu and then select use set.\r\nIf the set has a transmogrification for an item that is already transmogrified, the old transmogrification is lost.\r\nNote that same transmogrification restrictions apply when trying to use a set as in normal transmogrification.\r\n\r\nTo delete a set you can go to the set\'s menu and select delete set.', 1);

SET @STRING_ENTRY := 11100;
INSERT INTO `trinity_string` (`entry`, `content_default`) VALUES
(@STRING_ENTRY+0, 'Item transmogrified'),
(@STRING_ENTRY+1, 'Equipment slot is empty'),
(@STRING_ENTRY+2, 'Invalid source item selected'),
(@STRING_ENTRY+3, 'Source item does not exist'),
(@STRING_ENTRY+4, 'Destination item does not exist'),
(@STRING_ENTRY+5, 'Selected items are invalid'),
(@STRING_ENTRY+6, 'Not enough money'),
(@STRING_ENTRY+7, 'You don\'t have enough tokens'),
(@STRING_ENTRY+8, 'Transmogrifications removed'),
(@STRING_ENTRY+9, 'There are no transmogrifications'),
(@STRING_ENTRY+10, 'Invalid name inserted');
