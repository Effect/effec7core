/* Table structure for table `dynamic_teleporter` */

CREATE TABLE `dynamic_teleporter` (
  `entry` INT(11) UNSIGNED NOT NULL,
  `menu_parent` INT(11) UNSIGNED NOT NULL DEFAULT '0',
  `menu_sub` INT(11) NOT NULL DEFAULT '-1',
  `icon` TINYINT(3) UNSIGNED NOT NULL DEFAULT '0',
  `faction` INT(11) UNSIGNED NOT NULL DEFAULT '0',
  `name` CHAR(100) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `map` SMALLINT(5) UNSIGNED NOT NULL DEFAULT '0',
  `position_x` FLOAT NOT NULL DEFAULT '0',
  `position_y` FLOAT NOT NULL DEFAULT '0',
  `position_z` FLOAT NOT NULL DEFAULT '0',
  `position_o` FLOAT NOT NULL DEFAULT '0',
  PRIMARY KEY (`entry`)
) ENGINE=MYISAM DEFAULT CHARSET=utf8 CHECKSUM=1 DELAY_KEY_WRITE=1 ROW_FORMAT=DYNAMIC;